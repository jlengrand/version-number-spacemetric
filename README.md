# Version Number

## Introduction

A simple html file that returns the current DATE in string format using some simple javascript.

The string returned can be used as unique id when creating a database-update to avoid possible conflicts.

The used javascript 

```javascript 
var today = new Date();
var DD = today.getDate();
var MM = today.getMonth()+1; //January is 0!
var YYYY = today.getFullYear();
var HH = today.getHours();
var mm = today.getMinutes();

function twoNumbers(value){
    if(value<10){
        return value='0'+value;
    }
    return value;
}

mm = twoNumbers(mm);
HH = twoNumbers(HH);
DD = twoNumbers(DD);
MM = twoNumbers(MM);

// today = MM+'/'+DD+'/'+YYYY;
versionNumber = String(YYYY) + String(MM) + String(DD) + String(HH) + String(mm);
document.write(versionNumber);
```

## Setup

To get this running anywhere, simply drop the folder with in a webserver using the relevant apache/nginx configuration.
A sample nginx config is provided as example

## Author

Julien Lengrand-Lambert 
